/* Automatically generated nanopb header */
/* Generated by nanopb-0.3.9.8 at Wed Mar  9 09:54:10 2022. */

#ifndef PB_MISSING_FIELDS_PB_H_INCLUDED
#define PB_MISSING_FIELDS_PB_H_INCLUDED
#include <pb.h>

/* @@protoc_insertion_point(includes) */
#if PB_PROTO_HEADER_VERSION != 30
#error Regenerate this file with the current version of nanopb generator.
#endif

#ifdef __cplusplus
extern "C" {
#endif

/* Struct definitions */
typedef struct _AllFields {
    int32_t field1;
    int32_t field2;
    int32_t field3;
    int32_t field4;
    int32_t field5;
    int32_t field6;
    int32_t field7;
    int32_t field8;
    int32_t field9;
    int32_t field10;
    int32_t field11;
    int32_t field12;
    int32_t field13;
    int32_t field14;
    int32_t field15;
    int32_t field16;
    int32_t field17;
    int32_t field18;
    int32_t field19;
    int32_t field20;
    int32_t field21;
    int32_t field22;
    int32_t field23;
    int32_t field24;
    int32_t field25;
    int32_t field26;
    int32_t field27;
    int32_t field28;
    int32_t field29;
    int32_t field30;
    int32_t field31;
    int32_t field32;
    int32_t field33;
    int32_t field34;
    int32_t field35;
    int32_t field36;
    int32_t field37;
    int32_t field38;
    int32_t field39;
    int32_t field40;
    int32_t field41;
    int32_t field42;
    int32_t field43;
    int32_t field44;
    int32_t field45;
    int32_t field46;
    int32_t field47;
    int32_t field48;
    int32_t field49;
    int32_t field50;
    int32_t field51;
    int32_t field52;
    int32_t field53;
    int32_t field54;
    int32_t field55;
    int32_t field56;
    int32_t field57;
    int32_t field58;
    int32_t field59;
    int32_t field60;
    int32_t field61;
    int32_t field62;
    int32_t field63;
    int32_t field64;
/* @@protoc_insertion_point(struct:AllFields) */
} AllFields;

typedef struct _MissingField {
    int32_t field1;
    int32_t field2;
    int32_t field3;
    int32_t field4;
    int32_t field5;
    int32_t field6;
    int32_t field7;
    int32_t field8;
    int32_t field9;
    int32_t field10;
    int32_t field11;
    int32_t field12;
    int32_t field13;
    int32_t field14;
    int32_t field15;
    int32_t field16;
    int32_t field17;
    int32_t field18;
    int32_t field19;
    int32_t field20;
    int32_t field21;
    int32_t field22;
    int32_t field23;
    int32_t field24;
    int32_t field25;
    int32_t field26;
    int32_t field27;
    int32_t field28;
    int32_t field29;
    int32_t field30;
    int32_t field31;
    int32_t field32;
    int32_t field33;
    int32_t field34;
    int32_t field35;
    int32_t field36;
    int32_t field37;
    int32_t field38;
    int32_t field39;
    int32_t field40;
    int32_t field41;
    int32_t field42;
    int32_t field43;
    int32_t field44;
    int32_t field45;
    int32_t field46;
    int32_t field47;
    int32_t field48;
    int32_t field49;
    int32_t field50;
    int32_t field51;
    int32_t field52;
    int32_t field53;
    int32_t field54;
    int32_t field55;
    int32_t field56;
    int32_t field57;
    int32_t field58;
    int32_t field59;
    int32_t field60;
    int32_t field61;
    int32_t field62;
    int32_t field64;
/* @@protoc_insertion_point(struct:MissingField) */
} MissingField;

/* Default values for struct fields */

/* Initializer values for message structs */
#define AllFields_init_default                   {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
#define MissingField_init_default                {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
#define AllFields_init_zero                      {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
#define MissingField_init_zero                   {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}

/* Field tags (for use in manual encoding/decoding) */
#define AllFields_field1_tag                     1
#define AllFields_field2_tag                     2
#define AllFields_field3_tag                     3
#define AllFields_field4_tag                     4
#define AllFields_field5_tag                     5
#define AllFields_field6_tag                     6
#define AllFields_field7_tag                     7
#define AllFields_field8_tag                     8
#define AllFields_field9_tag                     9
#define AllFields_field10_tag                    10
#define AllFields_field11_tag                    11
#define AllFields_field12_tag                    12
#define AllFields_field13_tag                    13
#define AllFields_field14_tag                    14
#define AllFields_field15_tag                    15
#define AllFields_field16_tag                    16
#define AllFields_field17_tag                    17
#define AllFields_field18_tag                    18
#define AllFields_field19_tag                    19
#define AllFields_field20_tag                    20
#define AllFields_field21_tag                    21
#define AllFields_field22_tag                    22
#define AllFields_field23_tag                    23
#define AllFields_field24_tag                    24
#define AllFields_field25_tag                    25
#define AllFields_field26_tag                    26
#define AllFields_field27_tag                    27
#define AllFields_field28_tag                    28
#define AllFields_field29_tag                    29
#define AllFields_field30_tag                    30
#define AllFields_field31_tag                    31
#define AllFields_field32_tag                    32
#define AllFields_field33_tag                    33
#define AllFields_field34_tag                    34
#define AllFields_field35_tag                    35
#define AllFields_field36_tag                    36
#define AllFields_field37_tag                    37
#define AllFields_field38_tag                    38
#define AllFields_field39_tag                    39
#define AllFields_field40_tag                    40
#define AllFields_field41_tag                    41
#define AllFields_field42_tag                    42
#define AllFields_field43_tag                    43
#define AllFields_field44_tag                    44
#define AllFields_field45_tag                    45
#define AllFields_field46_tag                    46
#define AllFields_field47_tag                    47
#define AllFields_field48_tag                    48
#define AllFields_field49_tag                    49
#define AllFields_field50_tag                    50
#define AllFields_field51_tag                    51
#define AllFields_field52_tag                    52
#define AllFields_field53_tag                    53
#define AllFields_field54_tag                    54
#define AllFields_field55_tag                    55
#define AllFields_field56_tag                    56
#define AllFields_field57_tag                    57
#define AllFields_field58_tag                    58
#define AllFields_field59_tag                    59
#define AllFields_field60_tag                    60
#define AllFields_field61_tag                    61
#define AllFields_field62_tag                    62
#define AllFields_field63_tag                    63
#define AllFields_field64_tag                    64
#define MissingField_field1_tag                  1
#define MissingField_field2_tag                  2
#define MissingField_field3_tag                  3
#define MissingField_field4_tag                  4
#define MissingField_field5_tag                  5
#define MissingField_field6_tag                  6
#define MissingField_field7_tag                  7
#define MissingField_field8_tag                  8
#define MissingField_field9_tag                  9
#define MissingField_field10_tag                 10
#define MissingField_field11_tag                 11
#define MissingField_field12_tag                 12
#define MissingField_field13_tag                 13
#define MissingField_field14_tag                 14
#define MissingField_field15_tag                 15
#define MissingField_field16_tag                 16
#define MissingField_field17_tag                 17
#define MissingField_field18_tag                 18
#define MissingField_field19_tag                 19
#define MissingField_field20_tag                 20
#define MissingField_field21_tag                 21
#define MissingField_field22_tag                 22
#define MissingField_field23_tag                 23
#define MissingField_field24_tag                 24
#define MissingField_field25_tag                 25
#define MissingField_field26_tag                 26
#define MissingField_field27_tag                 27
#define MissingField_field28_tag                 28
#define MissingField_field29_tag                 29
#define MissingField_field30_tag                 30
#define MissingField_field31_tag                 31
#define MissingField_field32_tag                 32
#define MissingField_field33_tag                 33
#define MissingField_field34_tag                 34
#define MissingField_field35_tag                 35
#define MissingField_field36_tag                 36
#define MissingField_field37_tag                 37
#define MissingField_field38_tag                 38
#define MissingField_field39_tag                 39
#define MissingField_field40_tag                 40
#define MissingField_field41_tag                 41
#define MissingField_field42_tag                 42
#define MissingField_field43_tag                 43
#define MissingField_field44_tag                 44
#define MissingField_field45_tag                 45
#define MissingField_field46_tag                 46
#define MissingField_field47_tag                 47
#define MissingField_field48_tag                 48
#define MissingField_field49_tag                 49
#define MissingField_field50_tag                 50
#define MissingField_field51_tag                 51
#define MissingField_field52_tag                 52
#define MissingField_field53_tag                 53
#define MissingField_field54_tag                 54
#define MissingField_field55_tag                 55
#define MissingField_field56_tag                 56
#define MissingField_field57_tag                 57
#define MissingField_field58_tag                 58
#define MissingField_field59_tag                 59
#define MissingField_field60_tag                 60
#define MissingField_field61_tag                 61
#define MissingField_field62_tag                 62
#define MissingField_field64_tag                 64

/* Struct field encoding specification for nanopb */
extern const pb_field_t AllFields_fields[65];
extern const pb_field_t MissingField_fields[64];

/* Maximum encoded size of messages (where known) */
#define AllFields_size                           753
#define MissingField_size                        741

/* Message IDs (where set with "msgid" option) */
#ifdef PB_MSGID

#define MISSING_FIELDS_MESSAGES \


#endif

#ifdef __cplusplus
} /* extern "C" */
#endif
/* @@protoc_insertion_point(eof) */

#endif
