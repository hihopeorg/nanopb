/* Automatically generated nanopb header */
/* Generated by nanopb-0.3.9.8 at Wed Mar  9 09:54:03 2022. */

#ifndef PB_ENUMSIZES_PB_H_INCLUDED
#define PB_ENUMSIZES_PB_H_INCLUDED
#include <pb.h>

/* @@protoc_insertion_point(includes) */
#if PB_PROTO_HEADER_VERSION != 30
#error Regenerate this file with the current version of nanopb generator.
#endif

#ifdef __cplusplus
extern "C" {
#endif

/* Enum definitions */
typedef enum _UnpackedUint8 {
    UU8_MIN = 0,
    UU8_MAX = 255
} UnpackedUint8;
#define _UnpackedUint8_MIN UU8_MIN
#define _UnpackedUint8_MAX UU8_MAX
#define _UnpackedUint8_ARRAYSIZE ((UnpackedUint8)(UU8_MAX+1))
#define UnpackedUint8_UU8_MIN UU8_MIN
#define UnpackedUint8_UU8_MAX UU8_MAX

typedef enum _PackedUint8 {
    PU8_MIN = 0,
    PU8_MAX = 255
} pb_packed PackedUint8;
#define _PackedUint8_MIN PU8_MIN
#define _PackedUint8_MAX PU8_MAX
#define _PackedUint8_ARRAYSIZE ((PackedUint8)(PU8_MAX+1))
#define PackedUint8_PU8_MIN PU8_MIN
#define PackedUint8_PU8_MAX PU8_MAX

typedef enum _UnpackedInt8 {
    UI8_MIN = -128,
    UI8_MAX = 127
} UnpackedInt8;
#define _UnpackedInt8_MIN UI8_MIN
#define _UnpackedInt8_MAX UI8_MAX
#define _UnpackedInt8_ARRAYSIZE ((UnpackedInt8)(UI8_MAX+1))
#define UnpackedInt8_UI8_MIN UI8_MIN
#define UnpackedInt8_UI8_MAX UI8_MAX

typedef enum _PackedInt8 {
    PI8_MIN = -128,
    PI8_MAX = 127
} pb_packed PackedInt8;
#define _PackedInt8_MIN PI8_MIN
#define _PackedInt8_MAX PI8_MAX
#define _PackedInt8_ARRAYSIZE ((PackedInt8)(PI8_MAX+1))
#define PackedInt8_PI8_MIN PI8_MIN
#define PackedInt8_PI8_MAX PI8_MAX

typedef enum _UnpackedUint16 {
    UU16_MIN = 0,
    UU16_MAX = 65535
} UnpackedUint16;
#define _UnpackedUint16_MIN UU16_MIN
#define _UnpackedUint16_MAX UU16_MAX
#define _UnpackedUint16_ARRAYSIZE ((UnpackedUint16)(UU16_MAX+1))
#define UnpackedUint16_UU16_MIN UU16_MIN
#define UnpackedUint16_UU16_MAX UU16_MAX

typedef enum _PackedUint16 {
    PU16_MIN = 0,
    PU16_MAX = 65535
} pb_packed PackedUint16;
#define _PackedUint16_MIN PU16_MIN
#define _PackedUint16_MAX PU16_MAX
#define _PackedUint16_ARRAYSIZE ((PackedUint16)(PU16_MAX+1))
#define PackedUint16_PU16_MIN PU16_MIN
#define PackedUint16_PU16_MAX PU16_MAX

typedef enum _UnpackedInt16 {
    UI16_MIN = -32768,
    UI16_MAX = 32767
} UnpackedInt16;
#define _UnpackedInt16_MIN UI16_MIN
#define _UnpackedInt16_MAX UI16_MAX
#define _UnpackedInt16_ARRAYSIZE ((UnpackedInt16)(UI16_MAX+1))
#define UnpackedInt16_UI16_MIN UI16_MIN
#define UnpackedInt16_UI16_MAX UI16_MAX

typedef enum _PackedInt16 {
    PI16_MIN = -32768,
    PI16_MAX = 32767
} pb_packed PackedInt16;
#define _PackedInt16_MIN PI16_MIN
#define _PackedInt16_MAX PI16_MAX
#define _PackedInt16_ARRAYSIZE ((PackedInt16)(PI16_MAX+1))
#define PackedInt16_PI16_MIN PI16_MIN
#define PackedInt16_PI16_MAX PI16_MAX

/* Struct definitions */
typedef struct _PackedEnums {
    PackedUint8 u8_min;
    PackedUint8 u8_max;
    PackedInt8 i8_min;
    PackedInt8 i8_max;
    PackedUint16 u16_min;
    PackedUint16 u16_max;
    PackedInt16 i16_min;
    PackedInt16 i16_max;
/* @@protoc_insertion_point(struct:PackedEnums) */
} PackedEnums;

typedef struct _UnpackedEnums {
    UnpackedUint8 u8_min;
    UnpackedUint8 u8_max;
    UnpackedInt8 i8_min;
    UnpackedInt8 i8_max;
    UnpackedUint16 u16_min;
    UnpackedUint16 u16_max;
    UnpackedInt16 i16_min;
    UnpackedInt16 i16_max;
/* @@protoc_insertion_point(struct:UnpackedEnums) */
} UnpackedEnums;

/* Default values for struct fields */

/* Initializer values for message structs */
#define PackedEnums_init_default                 {_PackedUint8_MIN, _PackedUint8_MIN, _PackedInt8_MIN, _PackedInt8_MIN, _PackedUint16_MIN, _PackedUint16_MIN, _PackedInt16_MIN, _PackedInt16_MIN}
#define UnpackedEnums_init_default               {_UnpackedUint8_MIN, _UnpackedUint8_MIN, _UnpackedInt8_MIN, _UnpackedInt8_MIN, _UnpackedUint16_MIN, _UnpackedUint16_MIN, _UnpackedInt16_MIN, _UnpackedInt16_MIN}
#define PackedEnums_init_zero                    {_PackedUint8_MIN, _PackedUint8_MIN, _PackedInt8_MIN, _PackedInt8_MIN, _PackedUint16_MIN, _PackedUint16_MIN, _PackedInt16_MIN, _PackedInt16_MIN}
#define UnpackedEnums_init_zero                  {_UnpackedUint8_MIN, _UnpackedUint8_MIN, _UnpackedInt8_MIN, _UnpackedInt8_MIN, _UnpackedUint16_MIN, _UnpackedUint16_MIN, _UnpackedInt16_MIN, _UnpackedInt16_MIN}

/* Field tags (for use in manual encoding/decoding) */
#define PackedEnums_u8_min_tag                   1
#define PackedEnums_u8_max_tag                   2
#define PackedEnums_i8_min_tag                   3
#define PackedEnums_i8_max_tag                   4
#define PackedEnums_u16_min_tag                  5
#define PackedEnums_u16_max_tag                  6
#define PackedEnums_i16_min_tag                  7
#define PackedEnums_i16_max_tag                  8
#define UnpackedEnums_u8_min_tag                 1
#define UnpackedEnums_u8_max_tag                 2
#define UnpackedEnums_i8_min_tag                 3
#define UnpackedEnums_i8_max_tag                 4
#define UnpackedEnums_u16_min_tag                5
#define UnpackedEnums_u16_max_tag                6
#define UnpackedEnums_i16_min_tag                7
#define UnpackedEnums_i16_max_tag                8

/* Struct field encoding specification for nanopb */
extern const pb_field_t PackedEnums_fields[9];
extern const pb_field_t UnpackedEnums_fields[9];

/* Maximum encoded size of messages (where known) */
#define PackedEnums_size                         58
#define UnpackedEnums_size                       58

/* Message IDs (where set with "msgid" option) */
#ifdef PB_MSGID

#define ENUMSIZES_MESSAGES \


#endif

#ifdef __cplusplus
} /* extern "C" */
#endif
/* @@protoc_insertion_point(eof) */

#endif
