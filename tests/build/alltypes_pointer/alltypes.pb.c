/* Automatically generated nanopb constant definitions */
/* Generated by nanopb-0.3.9.8 at Wed Mar  9 09:53:58 2022. */

#include "alltypes.pb.h"

/* @@protoc_insertion_point(includes) */
#if PB_PROTO_HEADER_VERSION != 30
#error Regenerate this file with the current version of nanopb generator.
#endif

const int32_t SubMessage_substuff2_default = 2;
const uint32_t SubMessage_substuff3_default = 3u;
const int32_t Limits_int32_min_default = 2147483647;
const int32_t Limits_int32_max_default = -2147483647;
const uint32_t Limits_uint32_min_default = 4294967295u;
const uint32_t Limits_uint32_max_default = 0u;
const int64_t Limits_int64_min_default = 9223372036854775807ll;
const int64_t Limits_int64_max_default = -9223372036854775807ll;
const uint64_t Limits_uint64_min_default = 18446744073709551615ull;
const uint64_t Limits_uint64_max_default = 0ull;
const HugeEnum Limits_enum_min_default = HugeEnum_Positive;
const HugeEnum Limits_enum_max_default = HugeEnum_Negative;
const int32_t AllTypes_opt_int32_default = 4041;
const int64_t AllTypes_opt_int64_default = 4042ll;
const uint32_t AllTypes_opt_uint32_default = 4043u;
const uint64_t AllTypes_opt_uint64_default = 4044ull;
const int32_t AllTypes_opt_sint32_default = 4045;
const int64_t AllTypes_opt_sint64_default = 4046;
const bool AllTypes_opt_bool_default = false;
const uint32_t AllTypes_opt_fixed32_default = 4048u;
const int32_t AllTypes_opt_sfixed32_default = 4049;
const float AllTypes_opt_float_default = 4050;
const uint64_t AllTypes_opt_fixed64_default = 4051ull;
const int64_t AllTypes_opt_sfixed64_default = 4052ll;
const double AllTypes_opt_double_default = 4053;
const MyEnum AllTypes_opt_enum_default = MyEnum_Second;


const pb_field_t SubMessage_fields[4] = {
    PB_FIELD(  1, STRING  , REQUIRED, POINTER , FIRST, SubMessage, substuff1, substuff1, 0),
    PB_FIELD(  2, INT32   , REQUIRED, POINTER , OTHER, SubMessage, substuff2, substuff1, &SubMessage_substuff2_default),
    PB_FIELD(  3, FIXED32 , OPTIONAL, POINTER , OTHER, SubMessage, substuff3, substuff2, &SubMessage_substuff3_default),
    PB_LAST_FIELD
};

const pb_field_t EmptyMessage_fields[1] = {
    PB_LAST_FIELD
};

const pb_field_t Limits_fields[11] = {
    PB_FIELD(  1, INT32   , REQUIRED, POINTER , FIRST, Limits, int32_min, int32_min, &Limits_int32_min_default),
    PB_FIELD(  2, INT32   , REQUIRED, POINTER , OTHER, Limits, int32_max, int32_min, &Limits_int32_max_default),
    PB_FIELD(  3, UINT32  , REQUIRED, POINTER , OTHER, Limits, uint32_min, int32_max, &Limits_uint32_min_default),
    PB_FIELD(  4, UINT32  , REQUIRED, POINTER , OTHER, Limits, uint32_max, uint32_min, &Limits_uint32_max_default),
    PB_FIELD(  5, INT64   , REQUIRED, POINTER , OTHER, Limits, int64_min, uint32_max, &Limits_int64_min_default),
    PB_FIELD(  6, INT64   , REQUIRED, POINTER , OTHER, Limits, int64_max, int64_min, &Limits_int64_max_default),
    PB_FIELD(  7, UINT64  , REQUIRED, POINTER , OTHER, Limits, uint64_min, int64_max, &Limits_uint64_min_default),
    PB_FIELD(  8, UINT64  , REQUIRED, POINTER , OTHER, Limits, uint64_max, uint64_min, &Limits_uint64_max_default),
    PB_FIELD(  9, ENUM    , REQUIRED, POINTER , OTHER, Limits, enum_min, uint64_max, &Limits_enum_min_default),
    PB_FIELD( 10, ENUM    , REQUIRED, POINTER , OTHER, Limits, enum_max, enum_min, &Limits_enum_max_default),
    PB_LAST_FIELD
};

const pb_field_t AllTypes_fields[63] = {
    PB_FIELD(  1, INT32   , REQUIRED, POINTER , FIRST, AllTypes, req_int32, req_int32, 0),
    PB_FIELD(  2, INT64   , REQUIRED, POINTER , OTHER, AllTypes, req_int64, req_int32, 0),
    PB_FIELD(  3, UINT32  , REQUIRED, POINTER , OTHER, AllTypes, req_uint32, req_int64, 0),
    PB_FIELD(  4, UINT64  , REQUIRED, POINTER , OTHER, AllTypes, req_uint64, req_uint32, 0),
    PB_FIELD(  5, SINT32  , REQUIRED, POINTER , OTHER, AllTypes, req_sint32, req_uint64, 0),
    PB_FIELD(  6, SINT64  , REQUIRED, POINTER , OTHER, AllTypes, req_sint64, req_sint32, 0),
    PB_FIELD(  7, BOOL    , REQUIRED, POINTER , OTHER, AllTypes, req_bool, req_sint64, 0),
    PB_FIELD(  8, FIXED32 , REQUIRED, POINTER , OTHER, AllTypes, req_fixed32, req_bool, 0),
    PB_FIELD(  9, SFIXED32, REQUIRED, POINTER , OTHER, AllTypes, req_sfixed32, req_fixed32, 0),
    PB_FIELD( 10, FLOAT   , REQUIRED, POINTER , OTHER, AllTypes, req_float, req_sfixed32, 0),
    PB_FIELD( 11, FIXED64 , REQUIRED, POINTER , OTHER, AllTypes, req_fixed64, req_float, 0),
    PB_FIELD( 12, SFIXED64, REQUIRED, POINTER , OTHER, AllTypes, req_sfixed64, req_fixed64, 0),
    PB_FIELD( 13, DOUBLE  , REQUIRED, POINTER , OTHER, AllTypes, req_double, req_sfixed64, 0),
    PB_FIELD( 14, STRING  , REQUIRED, POINTER , OTHER, AllTypes, req_string, req_double, 0),
    PB_FIELD( 15, BYTES   , REQUIRED, POINTER , OTHER, AllTypes, req_bytes, req_string, 0),
    PB_FIELD( 16, MESSAGE , REQUIRED, POINTER , OTHER, AllTypes, req_submsg, req_bytes, &SubMessage_fields),
    PB_FIELD( 17, UENUM   , REQUIRED, POINTER , OTHER, AllTypes, req_enum, req_submsg, 0),
    PB_FIELD( 18, MESSAGE , REQUIRED, POINTER , OTHER, AllTypes, req_emptymsg, req_enum, &EmptyMessage_fields),
    PB_FIELD( 19, FIXED_LENGTH_BYTES, REQUIRED, POINTER , OTHER, AllTypes, req_fbytes, req_emptymsg, 0),
    PB_FIELD( 21, INT32   , REPEATED, POINTER , OTHER, AllTypes, rep_int32, req_fbytes, 0),
    PB_FIELD( 22, INT64   , REPEATED, POINTER , OTHER, AllTypes, rep_int64, rep_int32, 0),
    PB_FIELD( 23, UINT32  , REPEATED, POINTER , OTHER, AllTypes, rep_uint32, rep_int64, 0),
    PB_FIELD( 24, UINT64  , REPEATED, POINTER , OTHER, AllTypes, rep_uint64, rep_uint32, 0),
    PB_FIELD( 25, SINT32  , REPEATED, POINTER , OTHER, AllTypes, rep_sint32, rep_uint64, 0),
    PB_FIELD( 26, SINT64  , REPEATED, POINTER , OTHER, AllTypes, rep_sint64, rep_sint32, 0),
    PB_FIELD( 27, BOOL    , REPEATED, POINTER , OTHER, AllTypes, rep_bool, rep_sint64, 0),
    PB_FIELD( 28, FIXED32 , REPEATED, POINTER , OTHER, AllTypes, rep_fixed32, rep_bool, 0),
    PB_FIELD( 29, SFIXED32, REPEATED, POINTER , OTHER, AllTypes, rep_sfixed32, rep_fixed32, 0),
    PB_FIELD( 30, FLOAT   , REPEATED, POINTER , OTHER, AllTypes, rep_float, rep_sfixed32, 0),
    PB_FIELD( 31, FIXED64 , REPEATED, POINTER , OTHER, AllTypes, rep_fixed64, rep_float, 0),
    PB_FIELD( 32, SFIXED64, REPEATED, POINTER , OTHER, AllTypes, rep_sfixed64, rep_fixed64, 0),
    PB_FIELD( 33, DOUBLE  , REPEATED, POINTER , OTHER, AllTypes, rep_double, rep_sfixed64, 0),
    PB_FIELD( 34, STRING  , REPEATED, POINTER , OTHER, AllTypes, rep_string, rep_double, 0),
    PB_FIELD( 35, BYTES   , REPEATED, POINTER , OTHER, AllTypes, rep_bytes, rep_string, 0),
    PB_FIELD( 36, MESSAGE , REPEATED, POINTER , OTHER, AllTypes, rep_submsg, rep_bytes, &SubMessage_fields),
    PB_FIELD( 37, UENUM   , REPEATED, POINTER , OTHER, AllTypes, rep_enum, rep_submsg, 0),
    PB_FIELD( 38, MESSAGE , REPEATED, POINTER , OTHER, AllTypes, rep_emptymsg, rep_enum, &EmptyMessage_fields),
    PB_FIELD( 39, FIXED_LENGTH_BYTES, REPEATED, POINTER , OTHER, AllTypes, rep_fbytes, rep_emptymsg, 0),
    PB_FIELD( 41, INT32   , OPTIONAL, POINTER , OTHER, AllTypes, opt_int32, rep_fbytes, &AllTypes_opt_int32_default),
    PB_FIELD( 42, INT64   , OPTIONAL, POINTER , OTHER, AllTypes, opt_int64, opt_int32, &AllTypes_opt_int64_default),
    PB_FIELD( 43, UINT32  , OPTIONAL, POINTER , OTHER, AllTypes, opt_uint32, opt_int64, &AllTypes_opt_uint32_default),
    PB_FIELD( 44, UINT64  , OPTIONAL, POINTER , OTHER, AllTypes, opt_uint64, opt_uint32, &AllTypes_opt_uint64_default),
    PB_FIELD( 45, SINT32  , OPTIONAL, POINTER , OTHER, AllTypes, opt_sint32, opt_uint64, &AllTypes_opt_sint32_default),
    PB_FIELD( 46, SINT64  , OPTIONAL, POINTER , OTHER, AllTypes, opt_sint64, opt_sint32, &AllTypes_opt_sint64_default),
    PB_FIELD( 47, BOOL    , OPTIONAL, POINTER , OTHER, AllTypes, opt_bool, opt_sint64, &AllTypes_opt_bool_default),
    PB_FIELD( 48, FIXED32 , OPTIONAL, POINTER , OTHER, AllTypes, opt_fixed32, opt_bool, &AllTypes_opt_fixed32_default),
    PB_FIELD( 49, SFIXED32, OPTIONAL, POINTER , OTHER, AllTypes, opt_sfixed32, opt_fixed32, &AllTypes_opt_sfixed32_default),
    PB_FIELD( 50, FLOAT   , OPTIONAL, POINTER , OTHER, AllTypes, opt_float, opt_sfixed32, &AllTypes_opt_float_default),
    PB_FIELD( 51, FIXED64 , OPTIONAL, POINTER , OTHER, AllTypes, opt_fixed64, opt_float, &AllTypes_opt_fixed64_default),
    PB_FIELD( 52, SFIXED64, OPTIONAL, POINTER , OTHER, AllTypes, opt_sfixed64, opt_fixed64, &AllTypes_opt_sfixed64_default),
    PB_FIELD( 53, DOUBLE  , OPTIONAL, POINTER , OTHER, AllTypes, opt_double, opt_sfixed64, &AllTypes_opt_double_default),
    PB_FIELD( 54, STRING  , OPTIONAL, POINTER , OTHER, AllTypes, opt_string, opt_double, 0),
    PB_FIELD( 55, BYTES   , OPTIONAL, POINTER , OTHER, AllTypes, opt_bytes, opt_string, 0),
    PB_FIELD( 56, MESSAGE , OPTIONAL, POINTER , OTHER, AllTypes, opt_submsg, opt_bytes, &SubMessage_fields),
    PB_FIELD( 57, UENUM   , OPTIONAL, POINTER , OTHER, AllTypes, opt_enum, opt_submsg, &AllTypes_opt_enum_default),
    PB_FIELD( 58, MESSAGE , OPTIONAL, POINTER , OTHER, AllTypes, opt_emptymsg, opt_enum, &EmptyMessage_fields),
    PB_FIELD( 59, FIXED_LENGTH_BYTES, OPTIONAL, POINTER , OTHER, AllTypes, opt_fbytes, opt_emptymsg, 0),
    PB_ONEOF_FIELD(oneof,  60, MESSAGE , ONEOF, POINTER , OTHER, AllTypes, oneof_msg1, opt_fbytes, &SubMessage_fields),
    PB_ONEOF_FIELD(oneof,  61, MESSAGE , ONEOF, POINTER , UNION, AllTypes, oneof_msg2, opt_fbytes, &EmptyMessage_fields),
    PB_FIELD( 98, MESSAGE , REQUIRED, POINTER , OTHER, AllTypes, req_limits, oneof.oneof_msg2, &Limits_fields),
    PB_FIELD( 99, INT32   , REQUIRED, POINTER , OTHER, AllTypes, end, req_limits, 0),
    PB_FIELD(200, EXTENSION, OPTIONAL, CALLBACK, OTHER, AllTypes, extensions, end, 0),
    PB_LAST_FIELD
};




/* On some platforms (such as AVR), double is really float.
 * These are not directly supported by nanopb, but see example_avr_double.
 * To get rid of this error, remove any double fields from your .proto.
 */
PB_STATIC_ASSERT(sizeof(double) == 8, DOUBLE_MUST_BE_8_BYTES)

/* @@protoc_insertion_point(eof) */
