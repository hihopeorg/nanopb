/* Automatically generated nanopb header */
/* Generated by nanopb-0.3.9.8 at Wed Mar  9 09:54:11 2022. */

#ifndef PB_MULTIFILE2_PB_H_INCLUDED
#define PB_MULTIFILE2_PB_H_INCLUDED
#include <pb.h>

#include "multifile1.pb.h"

/* @@protoc_insertion_point(includes) */
#if PB_PROTO_HEADER_VERSION != 30
#error Regenerate this file with the current version of nanopb generator.
#endif

#ifdef __cplusplus
extern "C" {
#endif

/* Struct definitions */
typedef struct _Callback2Message {
    TestMessage tstmsg;
    SubMessage submsg;
/* @@protoc_insertion_point(struct:Callback2Message) */
} Callback2Message;

typedef struct _Enums {
    SignedEnum senum;
    UnsignedEnum uenum;
/* @@protoc_insertion_point(struct:Enums) */
} Enums;

typedef struct _OneofMessage {
    pb_size_t which_msgs;
    union {
        StaticMessage tstmsg;
    } msgs;
/* @@protoc_insertion_point(struct:OneofMessage) */
} OneofMessage;

/* Default values for struct fields */

/* Initializer values for message structs */
#define Callback2Message_init_default            {TestMessage_init_default, SubMessage_init_default}
#define OneofMessage_init_default                {0, {StaticMessage_init_default}}
#define Enums_init_default                       {_SignedEnum_MIN, _UnsignedEnum_MIN}
#define Callback2Message_init_zero               {TestMessage_init_zero, SubMessage_init_zero}
#define OneofMessage_init_zero                   {0, {StaticMessage_init_zero}}
#define Enums_init_zero                          {_SignedEnum_MIN, _UnsignedEnum_MIN}

/* Field tags (for use in manual encoding/decoding) */
#define Callback2Message_tstmsg_tag              1
#define Callback2Message_submsg_tag              2
#define Enums_senum_tag                          1
#define Enums_uenum_tag                          2
#define OneofMessage_tstmsg_tag                  1

/* Struct field encoding specification for nanopb */
extern const pb_field_t Callback2Message_fields[3];
extern const pb_field_t OneofMessage_fields[2];
extern const pb_field_t Enums_fields[3];

/* Maximum encoded size of messages (where known) */
#define Callback2Message_size                    (12 + TestMessage_size + SubMessage_size)
#define OneofMessage_size                        27
#define Enums_size                               14

/* Message IDs (where set with "msgid" option) */
#ifdef PB_MSGID

#define MULTIFILE2_MESSAGES \


#endif

#ifdef __cplusplus
} /* extern "C" */
#endif
/* @@protoc_insertion_point(eof) */

#endif
