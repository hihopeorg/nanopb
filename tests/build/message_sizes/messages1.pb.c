/* Automatically generated nanopb constant definitions */
/* Generated by nanopb-0.3.9.8 at Wed Mar  9 09:54:10 2022. */

#include "messages1.pb.h"

/* @@protoc_insertion_point(includes) */
#if PB_PROTO_HEADER_VERSION != 30
#error Regenerate this file with the current version of nanopb generator.
#endif



const pb_field_t MessageInfo_fields[3] = {
    PB_FIELD(  1, FIXED32 , REQUIRED, STATIC  , FIRST, MessageInfo, msg_id, msg_id, 0),
    PB_FIELD(  2, FIXED32 , OPTIONAL, STATIC  , OTHER, MessageInfo, interface_id, msg_id, 0),
    PB_LAST_FIELD
};

const pb_field_t MessageResponseInfo_fields[4] = {
    PB_FIELD(  1, FIXED64 , REQUIRED, STATIC  , FIRST, MessageResponseInfo, interface_id, interface_id, 0),
    PB_FIELD(  2, FIXED32 , REQUIRED, STATIC  , OTHER, MessageResponseInfo, seq, interface_id, 0),
    PB_FIELD(  3, FIXED32 , REQUIRED, STATIC  , OTHER, MessageResponseInfo, msg_id, seq, 0),
    PB_LAST_FIELD
};

const pb_field_t MessageHeader_fields[4] = {
    PB_FIELD(  1, MESSAGE , REQUIRED, STATIC  , FIRST, MessageHeader, info, info, &MessageInfo_fields),
    PB_FIELD(  2, MESSAGE , OPTIONAL, STATIC  , OTHER, MessageHeader, response_info, info, &MessageResponseInfo_fields),
    PB_FIELD(  3, MESSAGE , OPTIONAL, STATIC  , OTHER, MessageHeader, response, response_info, &MessageResponse_fields),
    PB_LAST_FIELD
};

const pb_field_t MessageResponse_fields[3] = {
    PB_FIELD(  1, UENUM   , REQUIRED, STATIC  , FIRST, MessageResponse, status, status, 0),
    PB_FIELD(  2, FIXED32 , REQUIRED, STATIC  , OTHER, MessageResponse, seq, status, 0),
    PB_LAST_FIELD
};



/* Check that field information fits in pb_field_t */
#if !defined(PB_FIELD_32BIT)
/* If you get an error here, it means that you need to define PB_FIELD_32BIT
 * compile-time option. You can do that in pb.h or on compiler command line.
 *
 * The reason you need to do this is that some of your messages contain tag
 * numbers or field sizes that are larger than what can fit in 8 or 16 bit
 * field descriptors.
 */
PB_STATIC_ASSERT((pb_membersize(MessageHeader, info) < 65536 && pb_membersize(MessageHeader, response_info) < 65536 && pb_membersize(MessageHeader, response) < 65536), YOU_MUST_DEFINE_PB_FIELD_32BIT_FOR_MESSAGES_MessageInfo_MessageResponseInfo_MessageHeader_MessageResponse)
#endif

#if !defined(PB_FIELD_16BIT) && !defined(PB_FIELD_32BIT)
/* If you get an error here, it means that you need to define PB_FIELD_16BIT
 * compile-time option. You can do that in pb.h or on compiler command line.
 *
 * The reason you need to do this is that some of your messages contain tag
 * numbers or field sizes that are larger than what can fit in the default
 * 8 bit descriptors.
 */
PB_STATIC_ASSERT((pb_membersize(MessageHeader, info) < 256 && pb_membersize(MessageHeader, response_info) < 256 && pb_membersize(MessageHeader, response) < 256), YOU_MUST_DEFINE_PB_FIELD_16BIT_FOR_MESSAGES_MessageInfo_MessageResponseInfo_MessageHeader_MessageResponse)
#endif


/* @@protoc_insertion_point(eof) */
