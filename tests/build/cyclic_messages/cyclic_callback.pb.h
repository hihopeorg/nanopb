/* Automatically generated nanopb header */
/* Generated by nanopb-0.3.9.8 at Wed Mar  9 09:54:02 2022. */

#ifndef PB_CYCLIC_CALLBACK_PB_H_INCLUDED
#define PB_CYCLIC_CALLBACK_PB_H_INCLUDED
#include <pb.h>

/* @@protoc_insertion_point(includes) */
#if PB_PROTO_HEADER_VERSION != 30
#error Regenerate this file with the current version of nanopb generator.
#endif

#ifdef __cplusplus
extern "C" {
#endif

/* Struct definitions */
typedef struct _Dictionary {
    pb_callback_t dictItem;
/* @@protoc_insertion_point(struct:Dictionary) */
} Dictionary;

typedef struct _KeyValuePair {
    char key[8];
    bool has_stringValue;
    char stringValue[8];
    bool has_intValue;
    int32_t intValue;
    bool has_dictValue;
    Dictionary dictValue;
    pb_callback_t treeValue;
/* @@protoc_insertion_point(struct:KeyValuePair) */
} KeyValuePair;

typedef struct _TreeNode {
    bool has_leaf;
    int32_t leaf;
    pb_callback_t left;
    pb_callback_t right;
/* @@protoc_insertion_point(struct:TreeNode) */
} TreeNode;

/* Default values for struct fields */

/* Initializer values for message structs */
#define TreeNode_init_default                    {false, 0, {{NULL}, NULL}, {{NULL}, NULL}}
#define Dictionary_init_default                  {{{NULL}, NULL}}
#define KeyValuePair_init_default                {"", false, "", false, 0, false, Dictionary_init_default, {{NULL}, NULL}}
#define TreeNode_init_zero                       {false, 0, {{NULL}, NULL}, {{NULL}, NULL}}
#define Dictionary_init_zero                     {{{NULL}, NULL}}
#define KeyValuePair_init_zero                   {"", false, "", false, 0, false, Dictionary_init_zero, {{NULL}, NULL}}

/* Field tags (for use in manual encoding/decoding) */
#define Dictionary_dictItem_tag                  1
#define KeyValuePair_key_tag                     1
#define KeyValuePair_stringValue_tag             2
#define KeyValuePair_intValue_tag                3
#define KeyValuePair_dictValue_tag               4
#define KeyValuePair_treeValue_tag               5
#define TreeNode_leaf_tag                        1
#define TreeNode_left_tag                        2
#define TreeNode_right_tag                       3

/* Struct field encoding specification for nanopb */
extern const pb_field_t TreeNode_fields[4];
extern const pb_field_t Dictionary_fields[2];
extern const pb_field_t KeyValuePair_fields[6];

/* Maximum encoded size of messages (where known) */
/* TreeNode_size depends on runtime parameters */
/* Dictionary_size depends on runtime parameters */
/* KeyValuePair_size depends on runtime parameters */

/* Message IDs (where set with "msgid" option) */
#ifdef PB_MSGID

#define CYCLIC_CALLBACK_MESSAGES \


#endif

#ifdef __cplusplus
} /* extern "C" */
#endif
/* @@protoc_insertion_point(eof) */

#endif
