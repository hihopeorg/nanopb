/* Automatically generated nanopb header */
/* Generated by nanopb-0.3.9.8 at Wed Mar  9 09:54:14 2022. */

#ifndef PB_SUBMESSAGE_PB_H_INCLUDED
#define PB_SUBMESSAGE_PB_H_INCLUDED
#include <pb.h>

/* @@protoc_insertion_point(includes) */
#if PB_PROTO_HEADER_VERSION != 30
#error Regenerate this file with the current version of nanopb generator.
#endif

#ifdef __cplusplus
extern "C" {
#endif

/* Struct definitions */
typedef PB_BYTES_ARRAY_T(16) submessage_data_t;
typedef struct _submessage {
    submessage_data_t data;
/* @@protoc_insertion_point(struct:submessage) */
} submessage;

/* Default values for struct fields */

/* Initializer values for message structs */
#define submessage_init_default                  {{0, {0}}}
#define submessage_init_zero                     {{0, {0}}}

/* Field tags (for use in manual encoding/decoding) */
#define submessage_data_tag                      1

/* Struct field encoding specification for nanopb */
extern const pb_field_t submessage_fields[2];

/* Maximum encoded size of messages (where known) */
#define submessage_size                          18

/* Message IDs (where set with "msgid" option) */
#ifdef PB_MSGID

#define SUBMESSAGE_MESSAGES \


#endif

#ifdef __cplusplus
} /* extern "C" */
#endif
/* @@protoc_insertion_point(eof) */

#endif
