/* Automatically generated nanopb constant definitions */
/* Generated by nanopb-0.3.9.8 at Wed Mar  9 09:54:17 2022. */

#include "callback_pointer.pb.h"

/* @@protoc_insertion_point(includes) */
#if PB_PROTO_HEADER_VERSION != 30
#error Regenerate this file with the current version of nanopb generator.
#endif



const pb_field_t SubMessage_fields[2] = {
    PB_FIELD(  1, INT32   , OPTIONAL, CALLBACK, FIRST, SubMessage, foo, foo, 0),
    PB_LAST_FIELD
};

const pb_field_t MainMessage_fields[2] = {
    PB_FIELD(  1, MESSAGE , OPTIONAL, POINTER , FIRST, MainMessage, bar, bar, &SubMessage_fields),
    PB_LAST_FIELD
};


/* @@protoc_insertion_point(eof) */
