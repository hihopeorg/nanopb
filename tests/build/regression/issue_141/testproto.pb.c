/* Automatically generated nanopb constant definitions */
/* Generated by nanopb-0.3.9.8 at Wed Mar  9 09:54:13 2022. */

#include "testproto.pb.h"

/* @@protoc_insertion_point(includes) */
#if PB_PROTO_HEADER_VERSION != 30
#error Regenerate this file with the current version of nanopb generator.
#endif



const pb_field_t SubMessage_fields[2] = {
    PB_FIELD(  1, INT32   , REPEATED, STATIC  , FIRST, SubMessage, array, array, 0),
    PB_LAST_FIELD
};

const pb_field_t OneOfMessage_fields[6] = {
    PB_FIELD(  1, INT32   , REQUIRED, STATIC  , FIRST, OneOfMessage, prefix, prefix, 0),
    PB_ONEOF_FIELD(values,   5, INT32   , ONEOF, STATIC  , OTHER, OneOfMessage, first, prefix, 0),
    PB_ONEOF_FIELD(values,   6, STRING  , ONEOF, STATIC  , UNION, OneOfMessage, second, prefix, 0),
    PB_ONEOF_FIELD(values,   7, MESSAGE , ONEOF, STATIC  , UNION, OneOfMessage, third, prefix, &SubMessage_fields),
    PB_FIELD( 99, INT32   , REQUIRED, STATIC  , OTHER, OneOfMessage, suffix, values.third, 0),
    PB_LAST_FIELD
};

const pb_field_t topMessage_fields[5] = {
    PB_FIELD(  1, INT32   , REQUIRED, STATIC  , FIRST, topMessage, start, start, 0),
    PB_ONEOF_FIELD(msg,   2, MESSAGE , ONEOF, STATIC  , OTHER, topMessage, msg1, start, &MyMessage1_fields),
    PB_ONEOF_FIELD(msg,   3, MESSAGE , ONEOF, STATIC  , UNION, topMessage, msg2, start, &MyMessage2_fields),
    PB_FIELD(  4, INT32   , REQUIRED, STATIC  , OTHER, topMessage, end, msg.msg2, 0),
    PB_LAST_FIELD
};

const pb_field_t MyMessage1_fields[4] = {
    PB_FIELD(  1, UINT32  , REQUIRED, STATIC  , FIRST, MyMessage1, n1, n1, 0),
    PB_FIELD(  2, UINT32  , REQUIRED, STATIC  , OTHER, MyMessage1, n2, n1, 0),
    PB_FIELD(  3, STRING  , REQUIRED, STATIC  , OTHER, MyMessage1, s, n2, 0),
    PB_LAST_FIELD
};

const pb_field_t MyMessage2_fields[3] = {
    PB_FIELD(  1, UINT32  , REQUIRED, STATIC  , FIRST, MyMessage2, num, num, 0),
    PB_FIELD(  2, BOOL    , REQUIRED, STATIC  , OTHER, MyMessage2, b, num, 0),
    PB_LAST_FIELD
};

const pb_field_t MyMessage3_fields[3] = {
    PB_FIELD(  1, BOOL    , REQUIRED, STATIC  , FIRST, MyMessage3, bbb, bbb, 0),
    PB_FIELD(  2, STRING  , REQUIRED, STATIC  , OTHER, MyMessage3, ss, bbb, 0),
    PB_LAST_FIELD
};

const pb_field_t MyMessage4_fields[5] = {
    PB_FIELD(  1, BOOL    , REQUIRED, STATIC  , FIRST, MyMessage4, bbbb, bbbb, 0),
    PB_FIELD(  2, STRING  , REQUIRED, STATIC  , OTHER, MyMessage4, sss, bbbb, 0),
    PB_FIELD(  3, UINT32  , REQUIRED, STATIC  , OTHER, MyMessage4, num, sss, 0),
    PB_FIELD(  4, UINT32  , REQUIRED, STATIC  , OTHER, MyMessage4, num2, num, 0),
    PB_LAST_FIELD
};


/* Check that field information fits in pb_field_t */
#if !defined(PB_FIELD_32BIT)
/* If you get an error here, it means that you need to define PB_FIELD_32BIT
 * compile-time option. You can do that in pb.h or on compiler command line.
 *
 * The reason you need to do this is that some of your messages contain tag
 * numbers or field sizes that are larger than what can fit in 8 or 16 bit
 * field descriptors.
 */
PB_STATIC_ASSERT((pb_membersize(OneOfMessage, values.third) < 65536 && pb_membersize(topMessage, msg.msg1) < 65536 && pb_membersize(topMessage, msg.msg2) < 65536), YOU_MUST_DEFINE_PB_FIELD_32BIT_FOR_MESSAGES_SubMessage_OneOfMessage_topMessage_MyMessage1_MyMessage2_MyMessage3_MyMessage4)
#endif

#if !defined(PB_FIELD_16BIT) && !defined(PB_FIELD_32BIT)
/* If you get an error here, it means that you need to define PB_FIELD_16BIT
 * compile-time option. You can do that in pb.h or on compiler command line.
 *
 * The reason you need to do this is that some of your messages contain tag
 * numbers or field sizes that are larger than what can fit in the default
 * 8 bit descriptors.
 */
PB_STATIC_ASSERT((pb_membersize(OneOfMessage, values.third) < 256 && pb_membersize(topMessage, msg.msg1) < 256 && pb_membersize(topMessage, msg.msg2) < 256), YOU_MUST_DEFINE_PB_FIELD_16BIT_FOR_MESSAGES_SubMessage_OneOfMessage_topMessage_MyMessage1_MyMessage2_MyMessage3_MyMessage4)
#endif


/* @@protoc_insertion_point(eof) */
