/* Automatically generated nanopb header */
/* Generated by nanopb-0.3.9.8 at Wed Mar  9 09:54:19 2022. */

#ifndef PB_TEST_PB_H_INCLUDED
#define PB_TEST_PB_H_INCLUDED
#include <pb.h>

/* @@protoc_insertion_point(includes) */
#if PB_PROTO_HEADER_VERSION != 30
#error Regenerate this file with the current version of nanopb generator.
#endif

#ifdef __cplusplus
extern "C" {
#endif

/* Enum definitions */
typedef enum _Reply_Result {
    Reply_Result_ERROR = 0,
    Reply_Result_OK = 1,
    Reply_Result_SOME_A = 2
} Reply_Result;
#define _Reply_Result_MIN Reply_Result_ERROR
#define _Reply_Result_MAX Reply_Result_SOME_A
#define _Reply_Result_ARRAYSIZE ((Reply_Result)(Reply_Result_SOME_A+1))

/* Struct definitions */
typedef struct _Error {
    int32_t code;
    pb_callback_t message;
/* @@protoc_insertion_point(struct:Error) */
} Error;

typedef struct _SubSubAMessage {
    char somestring[64];
/* @@protoc_insertion_point(struct:SubSubAMessage) */
} SubSubAMessage;

typedef struct _SubSubBMessage {
    char somestring[64];
/* @@protoc_insertion_point(struct:SubSubBMessage) */
} SubSubBMessage;

typedef struct _SubMessage {
    SubSubAMessage subsubmessageA;
    pb_callback_t subsubmessageB;
/* @@protoc_insertion_point(struct:SubMessage) */
} SubMessage;

typedef struct _Reply {
    Reply_Result result;
    Error error;
    SubMessage submessage;
/* @@protoc_insertion_point(struct:Reply) */
} Reply;

/* Default values for struct fields */

/* Initializer values for message structs */
#define Error_init_default                       {0, {{NULL}, NULL}}
#define SubSubAMessage_init_default              {""}
#define SubSubBMessage_init_default              {""}
#define SubMessage_init_default                  {SubSubAMessage_init_default, {{NULL}, NULL}}
#define Reply_init_default                       {_Reply_Result_MIN, Error_init_default, SubMessage_init_default}
#define Error_init_zero                          {0, {{NULL}, NULL}}
#define SubSubAMessage_init_zero                 {""}
#define SubSubBMessage_init_zero                 {""}
#define SubMessage_init_zero                     {SubSubAMessage_init_zero, {{NULL}, NULL}}
#define Reply_init_zero                          {_Reply_Result_MIN, Error_init_zero, SubMessage_init_zero}

/* Field tags (for use in manual encoding/decoding) */
#define Error_code_tag                           1
#define Error_message_tag                        2
#define SubSubAMessage_somestring_tag            1
#define SubSubBMessage_somestring_tag            1
#define SubMessage_subsubmessageA_tag            1
#define SubMessage_subsubmessageB_tag            2
#define Reply_result_tag                         1
#define Reply_error_tag                          2
#define Reply_submessage_tag                     3

/* Struct field encoding specification for nanopb */
extern const pb_field_t Error_fields[3];
extern const pb_field_t SubSubAMessage_fields[2];
extern const pb_field_t SubSubBMessage_fields[2];
extern const pb_field_t SubMessage_fields[3];
extern const pb_field_t Reply_fields[4];

/* Maximum encoded size of messages (where known) */
/* Error_size depends on runtime parameters */
#define SubSubAMessage_size                      66
#define SubSubBMessage_size                      66
/* SubMessage_size depends on runtime parameters */
/* Reply_size depends on runtime parameters */

/* Message IDs (where set with "msgid" option) */
#ifdef PB_MSGID

#define TEST_MESSAGES \


#endif

#ifdef __cplusplus
} /* extern "C" */
#endif
/* @@protoc_insertion_point(eof) */

#endif
