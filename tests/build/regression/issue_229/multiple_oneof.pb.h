/* Automatically generated nanopb header */
/* Generated by nanopb-0.3.9.8 at Wed Mar  9 09:54:15 2022. */

#ifndef PB_MULTIPLE_ONEOF_PB_H_INCLUDED
#define PB_MULTIPLE_ONEOF_PB_H_INCLUDED
#include <pb.h>

/* @@protoc_insertion_point(includes) */
#if PB_PROTO_HEADER_VERSION != 30
#error Regenerate this file with the current version of nanopb generator.
#endif

#ifdef __cplusplus
extern "C" {
#endif

/* Struct definitions */
typedef struct _MainMessage {
    pb_size_t which_oneof1;
    union {
        uint32_t oneof1_uint32;
    } oneof1;
    pb_size_t which_oneof2;
    union {
        uint32_t oneof2_uint32;
    } oneof2;
/* @@protoc_insertion_point(struct:MainMessage) */
} MainMessage;

/* Default values for struct fields */

/* Initializer values for message structs */
#define MainMessage_init_default                 {0, {0}, 0, {0}}
#define MainMessage_init_zero                    {0, {0}, 0, {0}}

/* Field tags (for use in manual encoding/decoding) */
#define MainMessage_oneof1_uint32_tag            1
#define MainMessage_oneof2_uint32_tag            2

/* Struct field encoding specification for nanopb */
extern const pb_field_t MainMessage_fields[3];

/* Maximum encoded size of messages (where known) */
#define MainMessage_size                         12

/* Message IDs (where set with "msgid" option) */
#ifdef PB_MSGID

#define MULTIPLE_ONEOF_MESSAGES \


#endif

#ifdef __cplusplus
} /* extern "C" */
#endif
/* @@protoc_insertion_point(eof) */

#endif
