/* Automatically generated nanopb header */
/* Generated by nanopb-0.3.9.8 at Wed Mar  9 09:54:01 2022. */

#ifndef PB_UNITTESTPROTO_PB_H_INCLUDED
#define PB_UNITTESTPROTO_PB_H_INCLUDED
#include <pb.h>

/* @@protoc_insertion_point(includes) */
#if PB_PROTO_HEADER_VERSION != 30
#error Regenerate this file with the current version of nanopb generator.
#endif

#ifdef __cplusplus
extern "C" {
#endif

/* Struct definitions */
typedef struct _CallbackArray {
    pb_callback_t data;
/* @@protoc_insertion_point(struct:CallbackArray) */
} CallbackArray;

typedef struct _StringPointerContainer {
    pb_size_t rep_str_count;
    char **rep_str;
/* @@protoc_insertion_point(struct:StringPointerContainer) */
} StringPointerContainer;

typedef PB_BYTES_ARRAY_T(16) BytesMessage_data_t;
typedef struct _BytesMessage {
    BytesMessage_data_t data;
/* @@protoc_insertion_point(struct:BytesMessage) */
} BytesMessage;

typedef struct _CallbackContainer {
    CallbackArray submsg;
/* @@protoc_insertion_point(struct:CallbackContainer) */
} CallbackContainer;

typedef struct _FloatArray {
    pb_size_t data_count;
    float data[10];
/* @@protoc_insertion_point(struct:FloatArray) */
} FloatArray;

typedef struct _IntegerArray {
    pb_size_t data_count;
    int32_t data[10];
/* @@protoc_insertion_point(struct:IntegerArray) */
} IntegerArray;

typedef struct _StringMessage {
    char data[10];
/* @@protoc_insertion_point(struct:StringMessage) */
} StringMessage;

typedef struct _CallbackContainerContainer {
    CallbackContainer submsg;
/* @@protoc_insertion_point(struct:CallbackContainerContainer) */
} CallbackContainerContainer;

typedef struct _IntegerContainer {
    IntegerArray submsg;
/* @@protoc_insertion_point(struct:IntegerContainer) */
} IntegerContainer;

/* Default values for struct fields */

/* Initializer values for message structs */
#define IntegerArray_init_default                {0, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0}}
#define FloatArray_init_default                  {0, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0}}
#define StringMessage_init_default               {""}
#define BytesMessage_init_default                {{0, {0}}}
#define CallbackArray_init_default               {{{NULL}, NULL}}
#define IntegerContainer_init_default            {IntegerArray_init_default}
#define CallbackContainer_init_default           {CallbackArray_init_default}
#define CallbackContainerContainer_init_default  {CallbackContainer_init_default}
#define StringPointerContainer_init_default      {0, NULL}
#define IntegerArray_init_zero                   {0, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0}}
#define FloatArray_init_zero                     {0, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0}}
#define StringMessage_init_zero                  {""}
#define BytesMessage_init_zero                   {{0, {0}}}
#define CallbackArray_init_zero                  {{{NULL}, NULL}}
#define IntegerContainer_init_zero               {IntegerArray_init_zero}
#define CallbackContainer_init_zero              {CallbackArray_init_zero}
#define CallbackContainerContainer_init_zero     {CallbackContainer_init_zero}
#define StringPointerContainer_init_zero         {0, NULL}

/* Field tags (for use in manual encoding/decoding) */
#define CallbackArray_data_tag                   1
#define StringPointerContainer_rep_str_tag       1
#define BytesMessage_data_tag                    1
#define CallbackContainer_submsg_tag             1
#define FloatArray_data_tag                      1
#define IntegerArray_data_tag                    1
#define StringMessage_data_tag                   1
#define CallbackContainerContainer_submsg_tag    1
#define IntegerContainer_submsg_tag              1

/* Struct field encoding specification for nanopb */
extern const pb_field_t IntegerArray_fields[2];
extern const pb_field_t FloatArray_fields[2];
extern const pb_field_t StringMessage_fields[2];
extern const pb_field_t BytesMessage_fields[2];
extern const pb_field_t CallbackArray_fields[2];
extern const pb_field_t IntegerContainer_fields[2];
extern const pb_field_t CallbackContainer_fields[2];
extern const pb_field_t CallbackContainerContainer_fields[2];
extern const pb_field_t StringPointerContainer_fields[2];

/* Maximum encoded size of messages (where known) */
#define IntegerArray_size                        110
#define FloatArray_size                          50
#define StringMessage_size                       12
#define BytesMessage_size                        18
/* CallbackArray_size depends on runtime parameters */
#define IntegerContainer_size                    112
/* CallbackContainer_size depends on runtime parameters */
/* CallbackContainerContainer_size depends on runtime parameters */
/* StringPointerContainer_size depends on runtime parameters */

/* Message IDs (where set with "msgid" option) */
#ifdef PB_MSGID

#define UNITTESTPROTO_MESSAGES \


#endif

#ifdef __cplusplus
} /* extern "C" */
#endif
/* @@protoc_insertion_point(eof) */

#endif
